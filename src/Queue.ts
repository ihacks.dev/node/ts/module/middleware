// Imports
import { Middleware } from "./Middleware";

/**
 * A middleware queue.
 */
export class Queue <Params extends any[] = any[]>
{
	
	/**
	 * The middlewares stored in this queue.
	 */
	public middlewares: Set<Middleware<Params>> = new Set<Middleware<Params>>()
	
	/**
	 * Add middlewares to the queue.
	 * @param middlewares The middlewares.
	 */
	public use (...middlewares: Middleware<Params>[]): this
	{
		middlewares.forEach(middleware => this.middlewares.add(middleware));
		return this;
	}
	
	/**
	 * Delete middlewares from the queue.
	 * @param middlewares The middlewares.
	 */
	public unuse (...middlewares: Middleware<Params>[]): this
	{
		middlewares.forEach(middleware => this.middlewares.delete(middleware));
		return this;
	}
	
	/**
	 * Run the middlewares stored in this queue.
	 * @param args The arguments to pass to the middleware.
	 */
	public runMiddleware (...args: Params): Promise<void>
	{
		return new Promise<void>(async (resolve, reject) => {
			
			let rejected: boolean = false;
			let rejectedValue: any;
			let it = this.middlewares.values();
			
			const iterator = (result: IteratorResult<Middleware<Params>, any>) => new Promise<void>(async (res, rej) => {
				if (rejected || result.done) return res();
				try
				{
					if (result.value instanceof Promise) await result.value;
					let v: any;
					if (typeof result.value === "function")
						if (result.value.constructor.name === "AsyncFunction") v = await result.value(() => iterator(it.next()), ...args);
						else v = result.value(() => iterator(it.next()), ...args);
					if (v instanceof Promise) await v;
					res();
				} catch (error)
				{
					rejected = true;
					rejectedValue = error;
					rej();
					reject(error);
				}
			}).catch(() => {});
			
			await iterator(it.next());
			resolve();
		});
	}
	
}
