// Imports
import { NextFunction } from "./NextFunction";

/**
 * A middleware function.
 */
export type Middleware <Params extends any[]> =
	(next: NextFunction, ...args: Params) => any | Promise<any>;
