/**
 * Calls the next value in the iterator.
 */
export type NextFunction = () => Promise<void>;
