// Imports
import typescript from "rollup-plugin-typescript2";
import { uglify } from "rollup-plugin-uglify";

export default {
	input: "./src/index.ts",
	output: {
		dir: "./build",
		format: "cjs",
		sourcemap: true
	},
	plugins: [
		typescript({ tsconfig: "./tsconfig.json" }),
		uglify()
	]
};
