# @ihacks.dev/middleware

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/481bcdd68a64467b9a6915b45f238323)](https://www.codacy.com/gl/ihacks.dev/middleware) &nbsp;
[![pipeline status](https://gitlab.com/ihacks.dev/node/ts/module/middleware/badges/master/pipeline.svg)](https://gitlab.com/ihacks.dev/node/ts/module/middleware/-/commits/master)

> A queue builder for middlewares.

## Installation

```bash
yarn add @ihacks.dev/middleware
# or
npm i -S @ihacks.dev/middleware
```

## Usage

Create a queue object.

```ts
const queue = new Queue<[ string, number, boolean ]>();
```

Add middlewares.

```ts
queue.add(async (next, str, num, bool) => {
	console.log(str, num, bool);
	await next();
	console.log("done");
})
```

Run the queue.

```ts
await queue.runMiddleware("a string", 123, true);
```

## License

See [LICENSE](./LICENSE).
